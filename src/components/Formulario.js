import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Formulario extends Component {
  ciudadRef = React.createRef()
  paisRef = React.createRef()

  buscarClima = e => {
    e.preventDefault()

    const respuesta = {
      ciudad: this.ciudadRef.current.value,
      pais: this.paisRef.current.value
    }

    this.props.datosConsulta(respuesta)
  }

  render() {
    return (
      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <form onSubmit={this.buscarClima}>
              <div className="input-field col s12 m8 l4 offset-m2">
                <input id="ciudad" type="text" ref={this.ciudadRef} />
                <label htmlFor="ciudad"> Ciudad </label>
              </div>
              <div className="input-field col s12 m8 l4 offset-m2">
                <select ref={this.paisRef}>
                  <option value="" defaultValue>
                    Elige un pais
                  </option>
                  <option value="PE">PERU</option>
                  <option value="AR">ARGENTINA</option>
                  <option value="MX">MEXICO</option>
                  <option value="CO">COLOMBIA</option>
                  <option value="CR">COSTARICA</option>
                  <option value="ES">ESPAÑA</option>
                  <option value="US">ESTADOS UNIDOS</option>
                </select>
                <label htmlFor="pais"> Pais </label>
              </div>
              <div className="input-field col s12 m8 l4 offset-m2 buscador">
                <input
                  type="submit"
                  className="waves-effect waves-light btn-large yellow accent-4"
                  value="Buscar..."
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

Formulario.propTypes = {
  datosConsulta: PropTypes.func.isRequired
}
