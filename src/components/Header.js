import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Header extends Component {
  render() {
    return (
      <div>
        <nav>
          <div className="nav-wrapper light-blue darken-2">
            <a href="#" className="brand-logo">
              {this.props.titulo}
            </a>
          </div>
        </nav>
      </div>
    )
  }
}

Header.proptypes = {
  titulo: PropTypes.string.isRequired
}
